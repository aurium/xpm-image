XPM-Image - Generate a xpm based on a math formula and a gradient pallet

Usage:

  $ image <width> <height> <expression> [<gradient steps>]

  Parameters:

    width and height ... The image size.
    
    expression ......... The math formula that will define each pixel color.
                         The formula must return a float inside [0..1], and
                         that value will be converted to a position on then
                         pallet. Any value outside the range will be
                         transparent.
                         You can use some variables on the formula:
                         x : the horizontal position of the computing pixel;
                         y : the vertical position of the computing pixel;
                         w : the width of the image;
                         h : the height of the image;
                         and any other environment variable.
    
    gradient steps ..... a comma separated list of (hexadecimal coded) colors.


Examples:

  1 - Fire-ball:
  $ image 40 40 '1-((x-19.5)^2+(y-19.5)^2)/398' 900,F90,FF9 > fire-ball.xpm
  Crates a 40x40 image with a ball with light yellow center turning to orange
  and ending in bloody red at the circle limits.

  2 - Textured Gradient:
  image 100 100 '( w/4 + x+y + ((\$RANDOM%w-w/2))/4 ) / (w*2.5)'
  Creates a inclined gradient with random disturbing on the pixel colors.


Change log:

v0.2
First public release, pallet generation rewritten to maximize the usage of the bc code and makes it runs hundred times faster.

v0.1
Pre public release, accept math formula, pallet gradient definition and mostly written in bash.
